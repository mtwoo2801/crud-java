/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.gui.util;



import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author fadil
 */
public class ConnectionUtility {
    private static Connection con;
    
    public static Connection getconnection() throws SQLException{
        if(con == null){
            MysqlDataSource ds = new MysqlDataSource();
            ds.setServerName("localhost");
            ds.setDatabaseName("akademik");
            ds.setUser("root");
            ds.setPassword("");
            con = ds.getConnection();
        }
        return con;
    }
}
